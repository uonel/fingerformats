module codeberg.org/uonel/mf2wf

go 1.17

require (
	github.com/gorilla/mux v1.8.0 // indirect
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa // indirect
	willnorris.com/go/microformats v1.1.1 // indirect
)
